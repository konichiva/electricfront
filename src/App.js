import React, { Component } from 'react';
import './App.css';
import Login from './components/login'
import AdminPanel from './components/adminPanel'
import {Route} from 'react-router-dom';
import Home from './components/home'

class App extends Component {
  render() {
    return (
      <div >
        <Route path="/" component={Home}/>
        <Route path="/admin" component={Login}/>
        <Route path="/adminPanel" component={AdminPanel}/>
      </div>
    );
  }
}

export default App;
