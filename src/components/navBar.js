import React from 'react';
import {Col} from 'react-bootstrap';
import {Row} from 'react-bootstrap';
import {Navbar} from 'react-bootstrap';
import {Nav} from 'react-bootstrap';
import {NavItem} from 'react-bootstrap';
import SideBar from './slideBar';
// import {NavDropdown} from 'react-bootstrap';


export default class NavBar extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
       menu:"",
       menuOpen:false,
       openSlide:true,
       subMenu:"",
       sidebarOpen: false
       };
       this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
  }
    componentDidMount(){
      this.getNavElement();
      this.onSetSidebarOpen();
    
    }
    onSetSidebarOpen=()=> {
 
      this.setState({sidebarOpen: true ,openSlide:true});
      console.log(this.state.sidebarOpen)
    }

    getNavElement=()=>{
      let menu = [];
      fetch('http://localhost:8080/electric.catalog/api/termek/menu',{
        method: 'GET',
          }).then(response => response.json()).then((response) => {
              if(response){
                response.map(element =>{
                  if(!menu.includes(element)){
                    menu.push(element)
                  }
                  this.setState({menu: menu})
                
                })
              }
           })  
    }

    getSlideMenuElements=(menuElement)=>{
      //console.log(menuElement)
      let subMenu = [];
      fetch('http://localhost:8080/electric.catalog/api/termek/almenu',{
        method: 'POST',
        body:  
        menuElement,
        headers: {"Content-Type": "application/json"}
          }).then(response => response.json()).then((response) => {
              if(response){
                response.map(element =>{
                  if(!subMenu.includes(element)){
                    subMenu.push(element)
                  }
                  this.setState({subMenuElement: subMenu, menuOpen:true})
                
                })
              }
           })  
    }
    
    render() {
      if(!this.state.menu) return null
      return (
        <div>
        <Col md={12}>
        <Row>
       <Navbar>
            <Navbar.Header>
                <Navbar.Brand>
                <a href="#home">React-Bootstrap</a>
                </Navbar.Brand>
            </Navbar.Header>
            <Nav>
                {this.state.menu.map((element, index)=>
                   <NavItem key={index} onClick={(event)=>{this.getSlideMenuElements(element)}}eventKey={index} href="#"> {element}</NavItem>
                )}
            </Nav>
        </Navbar>
        </Row>
        </Col>
          <Row> 
          <Col md={2}>
            <SideBar menuOpen={this.state.menuOpen} subMenuElement={this.state.subMenuElement} style={{'width': '15%'}}/>
           
            <div>
              
            </div>
          </Col>
         </Row>
     </div>
      );
    }
  }