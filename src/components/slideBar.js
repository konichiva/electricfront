import { slide as Menu } from 'react-burger-menu'
import {NavItem} from 'react-bootstrap';
import React, {Component} from 'react';
import '../App.css';
export default class SlideBar extends Component {

    

    getTermek=(event)=>{
        console.log(event)
    }

    render() {
        if(!this.props.subMenuElement)return ( <Menu isOpen={this.props.menuOpen}   noOverlay  stack>
                
            <div>Empty</div>
                </Menu>)
        return (
            <div>
                <Menu isOpen={this.props.menuOpen}  noOverlay  stack>
                
            {this.props.subMenuElement.map((element, index)=> <NavItem key={index} onClick={(event)=>{this.getTermek(element)}}eventKey={index} href="#"> {element}</NavItem>)}
                </Menu>
            </div>
        );
    }
}